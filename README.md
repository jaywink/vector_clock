Vector Clock
============

An analog clock showing an hour hand for different time zones.

Building
--------

This project is written in [PureScript](https://www.purescript.org/).  If you
have a PureScript environment, you can build it by doing:

```
spago bundle-app
```

as usual.  If you don't have a PureScript environment, you can build it by
doing:

```
yarn install
yarn run build
```

Configuring
-----------

Currently, the only way to configure is to modify the source.  The timezones to
be displayed are configure by the `timezones` variable in `src/Main.purs`.  The
clock face can be changed by modifying the `<g id="face">` element in
`index.html`.
